#version 330

/*
	Function that calculate the F0 value of the Fresnel equation

	The F0 value is the ratio at a 90� angle

	Params:
		- n1 : float : The incident environment ratio
		- n2 : float : The outgoing environment ratio

	Return:
		- The F0 value for these ratios
*/
float fresnel_F0 (float n1, float n2)
{
	return pow((n2 - n1) / (n2 + n1), 2);
}

/*
	Function that calculate the Fresnel ratio giving Theta the incident angle and F0

	Params:
		- cosTheta : float : The incident angle (with the normal and the view direction)
		- F0 : float : The F0 value

	Return:
		- The Fresnel ratio of reflectance (1 - this to have the refracted one)
*/
float fresnelSchlick(float cosTheta, float F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

in vec2 textureCoordinates;
in vec3 FragPos;
in vec3 normal;

uniform vec3 u_viewPos;
uniform samplerCube skybox;

uniform float ratio_i = 1.00;
uniform float ratio_o = 1.50;

uniform float r_chromaticDiff = -0.03;
uniform float g_chromaticDiff = 0.01;
uniform float b_chromaticDiff = 0.02;

out vec4 FragColor;


void main() {

	// Normalizing the normal
	vec3 norm = normalize(normal);
	// Getting the view direction vector
	vec3 viewDir = normalize(FragPos - u_viewPos);
	// Getting the theta incident angle
	float cos_theta_incident = dot(-viewDir, norm);

	// **********
	// Reflection
	// **********

	// Getting the reflection direction
    vec3 viewReflectionDir = reflect(viewDir, norm);
	// Getting the reflection color from the skybox
    vec3 reflected = texture(skybox, viewReflectionDir).rgb;

	//************
	// Refraction
	//************

	// Getting the ratios for the chromatic abberration
	float ratio_r = ratio_o + r_chromaticDiff;
	float ratio_g = ratio_o + g_chromaticDiff;
	float ratio_b = ratio_o + b_chromaticDiff;

	// Calculating the refraction vectors for each RGB colors with their ratio
    vec3 viewRefractionDir_b = refract(viewDir, norm, ratio_i / ratio_b);
    vec3 viewRefractionDir_g = refract(viewDir, norm, ratio_i / ratio_g);
    vec3 viewRefractionDir_r = refract(viewDir, norm, ratio_i / ratio_r);

	// Defining the colors for R, G and B giving their refraction direction and the corresponding skybox R, G and B colors
    vec3 refracted;
	refracted.b = texture(skybox, viewRefractionDir_b).b;
	refracted.g = texture(skybox, viewRefractionDir_g).g;
	refracted.r = texture(skybox, viewRefractionDir_r).r;

	// *************
	// Fresnel Ratio
	// *************

	// Calculating the Fresnesl ratio for reflectance and refraction
	float fresnel_reflect = fresnelSchlick(cos_theta_incident, fresnel_F0(ratio_i, ratio_o));

	// Giving the right amount of reflection and refraction color giving the fresnel ratio
	vec3 result = mix(refracted, reflected, fresnel_reflect);

	// Setting the color of the fragment
	FragColor = vec4(result, 1.0);
}