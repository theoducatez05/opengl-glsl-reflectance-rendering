#version 330

struct	ColorData {
	bool isTexture;
	vec3 color;
	sampler2D texture;
};

struct	Material {
	ColorData diffuse1;
	ColorData specular1;
	ColorData normal1;

	vec3 ambient;
	float shininess;
};

in vec2 textureCoordinates;

uniform Material material;

out vec4 FragColor;

void main()
{
	// Retrieve alpha
	float alpha = 1.0f;
	if (material.diffuse1.isTexture) {
		alpha = texture(material.diffuse1.texture, textureCoordinates).a;
	}

	// Skip pixel if too transparent
	if (alpha < 0.01)
		discard;

	// Get the diffuse color
	vec3 diffuse;

	if (material.diffuse1.isTexture) {												// If texture is a texture
		diffuse = vec3(texture(material.diffuse1.texture, textureCoordinates));
	}
	else {																			// If material color
		diffuse = material.diffuse1.color;
	}

	FragColor = vec4(diffuse, alpha);
}