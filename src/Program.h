#pragma once
/*****************************************************************//**
 * @file   Program.h
 * @brief  General singleton program class
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General includes
#include <memory>

// GL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Program includes
#include "utils/opengl/camera/FreeCamera.h"
#include "utils/opengl/GraphicObject.h"
#include "utils/opengl/Skybox.h"

/**
 * @brief The program singelton class
 */
class Program
{
private:

    /**
     * @brief The program singleton initialization
     *
     * It initializes the coordinate map
     */
    Program(){};
    ~Program() {};
    Program(const Program&) {};
    const Program& operator=(const Program&) {};

    static Program* instance;

public:

    /**
     * @brief Get the unique instance
     *
     * @return The program instance
     */
    static Program& getInstance();

    /**
     * @brief Refreshes the unique singleton instance
     *
     * Used when the game reloads
     */
    static void refreshInstance();

    // Window data
    int windowWidth = 1280;
    int windowHeight = 720;

    // Constant Folders
    std::string RESOURCE_FOLDER = "D:\\Etudes\\TCD\\Courses\\RR\\RR-Assigment_2\\resources\\";

    // Deltatime
    float deltaTime = 0.0f;
    long lastFrame = 0;

    // Camera
    std::unique_ptr<Camera> camera = std::make_unique<FreeCamera>(glm::vec3(1.0f, 1.30f, -5), glm::vec3(0, 1, 0));

    // Projection
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)windowWidth / windowHeight, 0.1f, 5000.0f);

    // Shaders
    std::shared_ptr<Shader> transmittanceShader;
    std::shared_ptr<Shader> cubeLightShader;

    // Objects
    std::shared_ptr<GraphicObject> teapot;
    std::shared_ptr<GraphicObject> sphere;
    std::shared_ptr<GraphicObject> cone;
    std::shared_ptr<GraphicObject> torus;
    std::shared_ptr<GraphicObject> cube2;
    std::shared_ptr<GraphicObject> diamond;

    int sphereType = 1;

    // Current object
    std::shared_ptr<GraphicObject> currentObject;
    std::string currentObjectType;

    // Skybox
    std::shared_ptr<Skybox> skybox;
    std::shared_ptr<Skybox> skybox2;
    std::shared_ptr<Skybox> skybox3;
    std::shared_ptr<Skybox> skybox4;
    std::shared_ptr<Skybox> skybox5;

    std::shared_ptr<Skybox> currentSkybox;

    // ETA
    float currentETA = 20.0f;

    /**
     * @brief Updates the program deltaTime
     *
     * @param elapsedTime Time between two frames
     */
    void updateDeltaTime(long elapsedTime);
};

