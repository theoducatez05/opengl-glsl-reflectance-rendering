#pragma once
/*****************************************************************//**
 * @file   IndexBuffer.h
 * @brief  The Index Buffer class
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General imports
#include <vector>

/**
 * @brief Represents an Index Buffer
 */
class IndexBuffer
{
private:

	unsigned int	m_bufferId;

	// The vertex count
	size_t			m_count;

public:

	/**
	 * @brief Constructor
	 *
	 * \param indices The indices vector
	 */
	IndexBuffer(const std::vector<unsigned int>& indices);
	IndexBuffer();
	~IndexBuffer();

	void bind() const;
	void unbind() const;

	// Getter and setters
	inline unsigned int getBufferId() const {
		return m_bufferId;
	}

	size_t getCount() const {
		return m_count;
	}
};
