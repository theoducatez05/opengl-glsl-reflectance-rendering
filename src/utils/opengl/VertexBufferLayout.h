#pragma once
/*****************************************************************//**
 * @file   VertexBufferLayout.h
 * @brief  The VertexBufferLayout header file
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General includes
#include <vector>

/**
 * @brief Represents a data layout item in the Vertex buffer data layout
 */
struct VertexBufferLayoutElement {
	unsigned int type;
	unsigned int count;
	unsigned int index;
	bool normalized;
};

/**
 * @brief Represents the data layout of a vertex buffer (used by the vertex array)
 */
class VertexBufferLayout
{
private:
	// The layout elements
	std::vector<VertexBufferLayoutElement> m_elements;
	// The current stride (taking into account the current elements)
	unsigned int m_stride;

public:
	VertexBufferLayout() : m_stride(0) {}

	// Adding data items to the layout
	void pushf(unsigned int count, unsigned int index);
	void pushui(unsigned int count, unsigned int index);
	void pushuc(unsigned int count, unsigned int index);
	void pushvec2f(unsigned int count, unsigned int index);
	void pushvec3f(unsigned int count, unsigned int index);
	void pushvec4f(unsigned int count, unsigned int index);

	/**
	 * @brief Sorts the elements by their position in the layout (to avoid holes)
	 *
	 */
	void sortElementsByPosition();
	/**
	 * @brief Get the GL type size of an element (a type)
	 *
	 * If we add new items types that can be added to the layout, we must
	 * complete this method with the new possible types
	 *
	 * @param type the GL type
	 * @return The size of it
	 */
	static unsigned int getSizeOfElement(unsigned int type);

	inline const std::vector<VertexBufferLayoutElement>getElements() const {return m_elements;}

	inline unsigned int getStride() const {return m_stride;}
};
