#include "Texture.h"

// General imports
#include <iostream>

// GL imports
#include <GL/glew.h>

// STB images import
#include "utils/stb_image.h"

// ASSIMP imports
#include <assimp/scene.h>

/**
* @brief Texture from a file
*
* @param m_filePath The file path
* @param type The texture type
* @param flip If needed to flip the texture
*/
Texture::Texture(const char* filePath, const TextureType type, bool flip) : m_filePath(filePath), m_type(type)
{
	int width, height, nrChannels;
	unsigned char* textureData = stbi_load(filePath, &width, &height, &nrChannels, 0);

	createTexture(textureData, type, width, height, nrChannels, flip);

	stbi_image_free(textureData);
}

/**
 * @brief Texture from embeded data (FBX)
 *
 * @param texture The embeded texture data
 * @param name The texture name
 * @param type The texture type
 * @param flip IF needed to flip the texture
 */
Texture::Texture(const aiTexture* texture, const char* filePath, TextureType type, bool flip) : m_filePath(filePath), m_type(type)
{
	unsigned char* textureData;
	int width, height, nrChannels;

	// load compressed texture data
	if (texture->mHeight == 0) {
		textureData = stbi_load_from_memory(reinterpret_cast<unsigned char*>(texture->pcData), \
			texture->mWidth, &width, &height, &nrChannels, 0);
	}
	// load uncompressed texture data
	else {
		textureData = stbi_load_from_memory(reinterpret_cast<unsigned char*>(texture->pcData), \
			texture->mWidth * texture->mHeight, &width, &height, &nrChannels, 0);
	}

	createTexture(textureData, type, width, height, nrChannels, flip);

	stbi_image_free(textureData);
}

Texture::~Texture()
{
	std::cout << "Deleting texture : " << m_id << std::endl;
	glDeleteTextures(1, &m_id);
}

/**
 * @brief Creates the texture
 *
 * @param textureData The texture data bytes
 * @param type The texture type
 * @param width The width
 * @param height The Height
 * @param nrChannels The number of channels
 * @param flip If needed to flip the texture
 */
void Texture::createTexture(unsigned char* textureData, const TextureType type, int width, int height, int nrChannels, bool flip)
{
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	stbi_set_flip_vertically_on_load(flip);

	glGenTextures(1, &m_id);
	glBindTexture(GL_TEXTURE_2D, m_id);
	if (textureData)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, textureData);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
}
