#pragma once
/*****************************************************************//**
 * @file   VertexArray.h
 * @brief  The VertexArray header file
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General includes
#include <memory>

// Program includes
#include "VertexBufferLayout.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"

/**
 * @brief Represents an OpenGL vertex array buffer
 */
class VertexArray
{
private:
	unsigned int m_bufferId;

public:
	VertexArray();
	~VertexArray();

	/**
	 * @brief Links the vertex array, the index buffer and the vertex buffer together (data layout)
	 *
	 * @param vertexBuffer
	 * @param indexBuffer
	 * @param vertexBufferLayout
	 */
	void link(const std::shared_ptr<VertexBuffer> vertexBuffer, const std::shared_ptr<IndexBuffer> indexBuffer, VertexBufferLayout& vertexBufferLayout);

	void bind() const;
	void unbind() const;
};
