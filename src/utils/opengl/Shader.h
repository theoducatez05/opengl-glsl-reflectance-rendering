#pragma once
/*****************************************************************//**
 * @file   Shader.h
 * @brief  The shader and ShaderSource struct header
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

 // General includes
#include <string>
#include <vector>
#include <unordered_map>

// GLM includes
#include <glm/glm.hpp>

/**
 * @brief Represents a shader source path and shader type structure
 */
struct ShaderSource
{
	std::string		shaderSource;
	unsigned int	shaderType;
};

/**
 * @brief Represents a Shader (vertex + fragment)
 */
class Shader
{
private:
	unsigned int m_programId;
	std::string	 m_vertexFilePath;
	std::string	 m_fragmentFilePath;

	// Cache for uniforms so we don't have to find them all the time
	mutable std::unordered_map<std::string, int> m_uniformLocationCache;

public:

	/**
	 * @brief Constructor
	 *
	 * \param vertexFilePath The vertex shader file path
	 * \param fragmentFilePath The fragment shader file path
	 */
	Shader(std::string vertexFilePath, std::string fragmentFilePath);
	~Shader();

	// Binding functions
	void bind() const;
	void unbind() const;

	// Utility uniform functions
	void setUniform1b(const std::string& name, bool value) const;
	void setUniform1i(const std::string& name, int value) const;
	void setUniform1f(const std::string& name, float value) const;
	void setUniform2fv(const std::string& name, glm::vec2 value) const;
	void setUniform3fv(const std::string& name, glm::vec3 value) const;
	void setUniform4fv(const std::string& name, glm::vec4 value) const;
	void setUniformMatrix4fv(const std::string& name, glm::mat4 value) const;

	/**
	 * @brief Returns the location of a uniform in the shader (or from cache)
	 *
	 * @param name The name of the unifrom
	 * @return The id of the uniform
	 */
	int getUniformLocation(const std::string& name) const;

private:
	Shader();
	/**
	 * @brief Parse the shader file
	 *
	 * @param shaderFilePath The shader (full) file Path
	 * @return The shader source code as a string
	 */
	std::string parseShader(std::string shaderFilePath);
	/**
	 * @brief Compiles the Shader soure code string
	 *
	 * @param shaderSources The shader source code string
	 * @return A boolean if everything was well done
	 */
	bool compileShaders(std::vector<ShaderSource> shaderSources);
	/**
	 * @brief Adds a shader to the shader Open GL program
	 *
	 * @param shaderSource
	 * @param shaderType
	 */
	void addShader(std::string& shaderSource, unsigned int shaderType);
};
