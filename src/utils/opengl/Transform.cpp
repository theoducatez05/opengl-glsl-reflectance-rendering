#include "Transform.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

/**
 * @brief Constructor
 *
 * \param position The position
 * \param orientation The orientation
 * \param scale The scale
 */
Transform::Transform(glm::vec3 position,
	glm::vec3 rotation,
	glm::vec3 scale) :
	m_position(position),
	m_rotation(rotation),
	m_scale(scale),
	m_modelTransform(defaultModelTransform)
{
	updateModelTransform();
}

Transform& Transform::operator=(Transform& transform)
{
	if (&transform != this) {
		m_position = transform.getPosition();
		m_rotation = transform.getRotation();
		m_scale = transform.getScale();
		m_modelTransform = transform.getModelTransform();
	}

	return *this;
}

/**
 * @brief Updates the model matrix with the current attributes
 *
 */
void Transform::updateModelTransform()
{
	m_rotation.x = fmodf(m_rotation.x, glm::radians(360.0f));
	m_rotation.y = fmodf(m_rotation.y, glm::radians(360.0f));
	m_rotation.z = fmodf(m_rotation.z, glm::radians(360.0f));

	m_modelTransform = defaultModelTransform;

	m_modelTransform = glm::translate(m_modelTransform, m_position);

	m_modelTransform = glm::rotate(m_modelTransform, m_rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	m_modelTransform = glm::rotate(m_modelTransform, m_rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	m_modelTransform = glm::rotate(m_modelTransform, m_rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

	m_modelTransform = glm::scale(m_modelTransform, m_scale);
}