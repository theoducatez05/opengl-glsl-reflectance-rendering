#pragma once
/*****************************************************************//**
 * @file   VertexBuffer.h
 * @brief  The VertexBuffer header file
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

/**
 * @brief Represents an OpenGL vertex buffer
 */
class VertexBuffer
{
private:
	unsigned int	m_bufferId;
	size_t			m_numVertices;

public:
	/**
	 * @brief Constructor
	 *
	 * \param numVertices The number of vertices
	 * \param vertices THe vertices
	 * \param verticeSize One vertex size
	 */
	VertexBuffer(size_t numVertices, const void* vertices, size_t verticeSize);
	~VertexBuffer();

	void bind() const;
	void unbind() const;

	// Getters
	inline unsigned int getBufferId() const {
		return m_bufferId;
	}

	size_t getNumVertices() const {
		return m_numVertices;
	}
};
