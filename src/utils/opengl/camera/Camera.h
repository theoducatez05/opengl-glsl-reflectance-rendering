#pragma once
/*****************************************************************//**
 * @file   Camera.h
 * @brief  The Camera class
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

#include <glm/glm.hpp>

/**
 * @brief Defines the different possible options for camera movement.
 */
enum class CameraMovement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    FORWARD_LEFT,
    FORWARD_RIGHT,
    BACKWARD_LEFT,
    BACKWARD_RIGHT,
    NONE
};

/**
 * @brief Defines the different camera types
 */
enum class CameraType {
    FREE,
    FPS,
    TOP_DOWN
};

/**
 * @brief The camera class
 */
class Camera
{
protected:

    // Camera Attributes
    glm::vec3   m_position;
    glm::vec3   m_front;
    glm::vec3   m_up;
    glm::vec3   m_right;
    glm::vec3   m_worldUp;

    // Euler Angles
    float       m_yaw;
    float       m_pitch;

    // Camera options
    float       m_movementSpeed;
    float       m_mouseSensitivity;

    // The camera type (for the childs)
    CameraType  m_type;

public:

    /**
     * @brief Returns the view matrix calculated using Euler Angles and the LookAt Matrix
     *
     * \return The view matrix
     */
    glm::mat4 GetViewMatrix();

    /**
    * @brief Processes input received from any keyboard-like input system.
    *
    * \param direction The direction
    * \param deltaTime The current deltatime
    */
    virtual void ProcessKeyboard(CameraMovement direction, float deltaTime) = 0;

    /**
    * @brief Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
    *
    * \param xoffset The x offset
    * \param yoffset The y offset
    * \param deltaTime The current deltatime
    * \param constrainPitch If we need to constrain the Pitch upward and downward
    */
    virtual void ProcessMouseMovement(float xoffset, float yoffset, float deltaTime, bool constrainPitch = true) = 0;

    /**
     * @brief Updates the camera vectors
     *
     */
    virtual void updateCameraVectors();

    // Getters
    inline CameraType getType(){ return m_type; }
    inline glm::vec3 getPosition() { return m_position; }

protected:
    /**
     * @brief Constructor
     *
     * \param initialPosition Initial position of the camera
     * \param up The UP vector
     * \param yaw The initial yaw
     * \param pitch The initial Pitch
     */
    Camera(glm::vec3 position, glm::vec3 up, float yaw, float pitch);
};

