#include "FreeCamera.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

/**
 * @brief Constructor
 *
 * \param initialPosition Initial position of the camera
 * \param up The UP vector
 * \param yaw The initial yaw
 * \param pitch The initial Pitch
 */FreeCamera::FreeCamera(glm::vec3 position, glm::vec3 up, float yaw, float pitch):
    Camera(position, up, yaw, pitch)
{
    m_type = CameraType::FREE;
    m_mouseSensitivity = FREE_CAMERA_DEFAULT_SENSITIVITY;
    m_movementSpeed = FREE_CAMERA_DEFAULT_SPEED;
    updateCameraVectors();
}

 /**
 * @brief Processes input received from any keyboard-like input system.
 *
 * \param direction The direction
 * \param deltaTime The current deltatime
 */
 void FreeCamera::ProcessKeyboard(CameraMovement direction, float deltaTime)
{
    float velocity = m_movementSpeed * deltaTime;

    // Update camera position
    if (direction == CameraMovement::FORWARD_LEFT) {
        m_position += m_front * velocity;
        m_position -= m_right * velocity;
    }
    else if (direction == CameraMovement::FORWARD_RIGHT) {
        m_position += m_front * velocity;
        m_position += m_right * velocity;
    }
    else if (direction == CameraMovement::BACKWARD_LEFT) {
        m_position -= m_front * velocity;
        m_position -= m_right * velocity;
    }
    else if (direction == CameraMovement::BACKWARD_RIGHT) {
        m_position -= m_front * velocity;
        m_position += m_right * velocity;
    }
    else if (direction == CameraMovement::FORWARD)
        m_position += m_front * velocity;
    else if (direction == CameraMovement::BACKWARD)
        m_position -= m_front * velocity;
    else if (direction == CameraMovement::LEFT)
        m_position -= m_right * velocity;
    else if (direction == CameraMovement::RIGHT)
        m_position += m_right * velocity;

    updateCameraVectors();
}

/**
 * @brief Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
 *
 * \param xoffset The x offset
 * \param yoffset The y offset
 * \param deltaTime The current deltatime
 * \param constrainPitch If we need to constrain the Pitch upward and downward
 */
void FreeCamera::ProcessMouseMovement(float xoffset, float yoffset, float deltaTime, bool constrainPitch)
{
    xoffset *= m_mouseSensitivity * deltaTime;
    yoffset *= m_mouseSensitivity * deltaTime;

    m_yaw += xoffset;
    m_pitch += yoffset;

    // make sure that when pitch is out of bounds, screen doesn't get flipped
    if (constrainPitch)
    {
        if (m_pitch > 89.0f)
            m_pitch = 89.0f;
        if (m_pitch < -89.0f)
            m_pitch = -89.0f;
    }

    // update Front, Right and Up Vectors using the updated Euler angles
    updateCameraVectors();
}