#pragma once
/*****************************************************************//**
 * @file   Material.h
 * @brief  The Material class
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General imports
#include <string>

// GLM imports
#include <glm/glm.hpp>

/**
 * @brief Represents an OpenGl Material
 */
class Material {
private:

	std::string	m_name;

	// Material values
	glm::vec3	m_diffuse;
	glm::vec3	m_specular;
	glm::vec3	m_ambient;
	float		m_shininess;

public:
	/**
	 * @brief Constructor
	 *
	 * \param name The name
	 * \param diffuse Diffuse value
	 * \param specular Specular value
	 * \param ambient Ambient value
	 * \param shininess Shininess value
	 */
	Material(std::string const name = "",
		glm::vec3 const diffuse = glm::vec3(.3f, .3f, .3f), \
		glm::vec3 const specular = glm::vec3(0.3f, 0.3f, 0.3f), \
		glm::vec3 const ambient = glm::vec3(0.5f, 0.5f, 0.5f), \
		float const shininess = 16.0f);
	Material(Material const& src);
	~Material();

	Material& operator=(Material const& rhs);

	// Getters and setters
	void inline setName(const std::string name) { m_name = name; }
	void inline setDiffuse(const glm::vec3 diffuse) { m_diffuse = diffuse; }
	void inline setSpecular(const glm::vec3 specular) { m_specular = specular; }
	void inline setAmbient(const glm::vec3 ambiant) { m_ambient = ambiant; }
	void inline setShininess(const float shininess) { m_shininess = shininess; }

	std::string inline getName() const { return m_name; }
	glm::vec3 inline getDiffuse() const { return m_diffuse; }
	glm::vec3 inline getSpecular() const { return m_specular; }
	glm::vec3 inline getAmbient() const { return m_ambient; }
	float inline getShininess() const { return m_shininess; }
};

