#include "Shader.h"

// General includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

// GL includes
#include <GL/glew.h>

// GLM includes
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

/**
* @brief Constructor
*
* \param vertexFilePath The vertex shader file path
* \param fragmentFilePath The fragment shader file path
*/
Shader::Shader(std::string vertexFilePath, std::string fragmentFilePath) :
	m_vertexFilePath(vertexFilePath),
	m_fragmentFilePath(fragmentFilePath)
{
	// Parse shader files
	std::vector<ShaderSource> shaderSources;
	shaderSources.push_back({ Shader::parseShader(vertexFilePath), GL_VERTEX_SHADER });
	shaderSources.push_back({ Shader::parseShader(fragmentFilePath), GL_FRAGMENT_SHADER });

	// Creating the Shader program ID
	m_programId = glCreateProgram();
	if (m_programId == 0) {
		fprintf(stderr, "Error creating shader program\n");
		exit(1);
	}

	// Compile shaders
	compileShaders(shaderSources);
}

Shader::~Shader()
{
	glDeleteProgram(m_programId);
}

/**
 * @brief Parse the shader file
 *
 * @param shaderFilePath The shader (full) file Path
 * @return The shader source code as a string
 */
std::string Shader::parseShader(std::string shaderFilePath)
{
	std::string shaderSource;
	std::ifstream shaderFile;

	// Ensure ifstream objects can throw exceptions:
	shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try
	{
		// Open files
		shaderFile.open(shaderFilePath);
		std::stringstream shaderStream;
		// Read file's buffer contents into streams
		shaderStream << shaderFile.rdbuf();
		// Close file handlers
		shaderFile.close();
		// Convert stream into string
		shaderSource = shaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ : " << shaderFilePath << std::endl;
		exit(1);
	}

	return shaderSource;
}

/**
 * @brief Adds a shader to the shader Open GL program
 *
 * @param shaderSource
 * @param shaderType
 */
void Shader::addShader(std::string& shaderSourceString, unsigned int shaderType)
{
	const char* shaderSource = shaderSourceString.c_str();

	// Create the shader object
	GLuint shaderObj = glCreateShader(shaderType);
	if (shaderObj == 0) {
		std::cout << "ERROR::SHADER::ERROR_CREATING_SHADER_TYPE " << shaderType << std::endl;
		exit(1);
	}

	// Bind the source code to the shader
	glShaderSource(shaderObj, 1, &shaderSource, NULL);
	// Compile the shader and check for errors
	glCompileShader(shaderObj);
	int success;
	// Check for shader related errors using glGetShaderiv
	glGetShaderiv(shaderObj, GL_COMPILE_STATUS, &success);
	if (!success) {
		char infoLog[1024];
		glGetShaderInfoLog(shaderObj, 1024, NULL, infoLog);
		std::cout << "ERROR::SHADER::" << shaderType << "::COMPILATION_FAILED\n" << infoLog << std::endl;
		exit(1);
	}

	// Attach the compiled shader object to the program object
	glAttachShader(m_programId, shaderObj);
	glDeleteShader(shaderObj);
}

/**
 * @brief Compiles the Shader soure code string
 *
 * @param shaderSources The shader source code string
 * @return A boolean if everything was well done
 */
bool Shader::compileShaders(std::vector<ShaderSource> shaderSources)
{
	// Create objects and link them to the program
	for (size_t i = 0; i < shaderSources.size(); ++i) {
		addShader(shaderSources[i].shaderSource, shaderSources[i].shaderType);
	}

	GLint success = 0;
	GLchar errorLog[1024] = { 0 };

	// Link the program after all compilation / shaderAttach
	glLinkProgram(m_programId);
	// Check for program related errors using glGetProgramiv
	glGetProgramiv(m_programId, GL_LINK_STATUS, &success);
	if (success == 0) {
		glGetProgramInfoLog(m_programId, sizeof(errorLog), NULL, errorLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << errorLog << std::endl;
		exit(1);
	}

	glValidateProgram(m_programId);
	// Check for program related errors using glGetProgramiv
	glGetProgramiv(m_programId, GL_VALIDATE_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(m_programId, sizeof(errorLog), NULL, errorLog);
		std::cout << "ERROR::SHADER::PROGRAM::INVALID_SHADER_PROGRAM\n" << errorLog << std::endl;
		exit(1);
	}
	// Finally, use the linked shader program
	glUseProgram(m_programId);

	return true;
}

/**
 * @brief Returns the location of a uniform in the shader (or from cache)
 *
 * @param name The name of the unifrom
 * @return The id of the uniform
 */
int Shader::getUniformLocation(const std::string& name) const
{

	if (m_uniformLocationCache.find(name) != m_uniformLocationCache.end()) {
		return m_uniformLocationCache[name];
	}

	int location = glGetUniformLocation(m_programId, name.c_str());
	if (location == -1) {
		std::cout << "WARNING::SHADER::PROGRAM::UNIFORM_NOT_FOUND:: Uniform " << name << " NOT FOUND" << std::endl;
	}
	else {
		m_uniformLocationCache[name] = location;
	}
	return location;
}

void Shader::setUniform1b(const std::string& name, bool value) const
{
	glUniform1i(getUniformLocation(name), value);
}
void Shader::setUniform1i(const std::string& name, int value) const
{
	glUniform1i(getUniformLocation(name), value);
}
void Shader::setUniform1f(const std::string& name, float value) const
{
	glUniform1f(getUniformLocation(name), value);
}
void Shader::setUniform2fv(const std::string& name, glm::vec2 value) const
{
	glUniform2fv(getUniformLocation(name), 1, glm::value_ptr(value));
}
void Shader::setUniform3fv(const std::string& name, glm::vec3 value) const
{
	glUniform3fv(getUniformLocation(name), 1, glm::value_ptr(value));
}
void Shader::setUniform4fv(const std::string& name, glm::vec4 value) const
{
	glUniform4fv(getUniformLocation(name), 1, glm::value_ptr(value));
}
void Shader::setUniformMatrix4fv(const std::string& name, glm::mat4 value) const
{
	glUniformMatrix4fv(getUniformLocation(name), 1, false, glm::value_ptr(value));
}

void Shader::bind() const
{
	glUseProgram(m_programId);
}

void Shader::unbind() const
{
	glUseProgram(0);
}