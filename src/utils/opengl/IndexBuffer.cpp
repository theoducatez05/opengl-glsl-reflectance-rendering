#include "IndexBuffer.h"

// General imports
#include <vector>
#include <iostream>

// GL imports
#include<GL/glew.h>

IndexBuffer::IndexBuffer() : m_count(0), m_bufferId(0) {

}

/**
 * @brief Constructor
 *
 * \param indices The indices vector
 */
IndexBuffer::IndexBuffer(const std::vector<unsigned int>& indices) : m_count(indices.size()) {
	// Generate the index buffer
	glGenBuffers(1, &m_bufferId);
	// Binding the buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufferId);
	// Create the space needed for our data in the buffer and load vertices directly
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_count * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
}

IndexBuffer::~IndexBuffer() {
	if (m_bufferId != 0) {
		glDeleteBuffers(1, &m_bufferId);
		std::cout << "IndexBuffer : " << m_bufferId << " deleted" << std::endl;
	}
}

void IndexBuffer::bind() const {
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufferId);
}

void IndexBuffer::unbind() const {
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}