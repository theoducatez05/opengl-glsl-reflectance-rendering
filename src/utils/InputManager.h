#pragma once
/*****************************************************************//**
 * @file   InputManager.h
 * @brief The input Manager header file
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// Program includes
#include "Program.h"

/**
 * @brief The singelton class managing inputs in the game
 */
class InputManager
{
private:

    InputManager() {};
    ~InputManager() {};
    InputManager(const InputManager&) {};
    const InputManager& operator=(const InputManager&) {};

public:

    /**
     * @brief The singleton get instance method
     *
     * @return The instance
     */
    static InputManager& getInstance();

    // Keys states
    bool keyPressed[256] = { false };

    // Mouse position
    float mousePositionX = Program::getInstance().windowWidth / 2.0f;
    float mousePositionY = Program::getInstance().windowHeight / 2.0f;

    // If the player needs the mouse
    bool inMenu = true;

    /**
     * @brief When a key is released
     *
     * @param key The key
     */
    static void keyUp(unsigned char key, int x, int y);
    /**
     * @brief When a key is pressed but doesn't need real time reaction
     *
     * @param key The key
     */
    static void keyPress(unsigned char key, int x, int y);

    /**
     * @brief Process user keyboard inputs.
     *
     */
    static void processKeyInputs();

    /**
     * @brief Mouse movement callback on FPS mode
     *
     * @param x X position
     * @param y Y position
     */
    static void mouseMoveFPS(int x, int y);
    /**
     * @brief Mouse movement callback in menus
     */
    static void mouseMove(int x, int y) {};
};