#include "InputManager.h"

#include <GL/freeglut.h>

#include "glm/gtc/constants.hpp"

#include "utils/opengl/camera/FreeCamera.h"

#include <iostream>

/**
* @brief The singleton get instance method
*
* @return The instance
*/
InputManager& InputManager::getInstance()
{
	static InputManager instance;
	return instance;
}

/**
* @brief When a key is released
*
* @param key The key
*/
void InputManager::keyUp(unsigned char key, int x, int y) {

	getInstance().keyPressed[key] = false;

	glutPostRedisplay();
}

/**
 * @brief When a key is pressed but doesn't need real time reaction
 *
 * @param key The key
 */
void InputManager::keyPress(unsigned char key, int x, int y)
{
	getInstance().keyPressed[key] = true;

	if (getInstance().keyPressed[27]) {
		if (getInstance().inMenu) {
			glutSetCursor(GLUT_CURSOR_INHERIT);
			glutPassiveMotionFunc(mouseMove);
			getInstance().inMenu = false;
		}
		else {
			glutSetCursor(GLUT_CURSOR_NONE);
			glutPassiveMotionFunc(mouseMoveFPS);
			getInstance().inMenu = true;
		}
	}

	if (getInstance().keyPressed['c']) {
		if (Program::getInstance().currentObject == Program::getInstance().sphere && Program::getInstance().sphereType == 1) {
			Program::getInstance().currentETA = 1.15f;
			Program::getInstance().sphereType = 2;
		}
		else if (Program::getInstance().currentObject == Program::getInstance().sphere && Program::getInstance().sphereType == 2) {
			Program::getInstance().currentETA = 1.52f;
			Program::getInstance().sphereType = 0;
		}
		else if (Program::getInstance().currentObject == Program::getInstance().sphere && Program::getInstance().sphereType == 0) {
			Program::getInstance().currentETA = 1.52f;
			Program::getInstance().currentObject = Program::getInstance().cube2;
		}
		else if (Program::getInstance().currentObject == Program::getInstance().cube2) {
			Program::getInstance().currentETA = 1.52f;
			Program::getInstance().currentObject = Program::getInstance().cone;
		}
		else if (Program::getInstance().currentObject == Program::getInstance().cone) {
			Program::getInstance().currentETA = 1.52f;
			Program::getInstance().currentObject = Program::getInstance().torus;
		}
		else if (Program::getInstance().currentObject == Program::getInstance().torus) {
			Program::getInstance().currentETA = 2.52f;
			Program::getInstance().currentObject = Program::getInstance().diamond;
		}
		else if (Program::getInstance().currentObject == Program::getInstance().diamond) {
			Program::getInstance().currentETA = 1.52f;
			Program::getInstance().currentObject = Program::getInstance().teapot;
		}
		else if (Program::getInstance().currentObject == Program::getInstance().teapot) {
			Program::getInstance().currentETA = 20.0f;
			Program::getInstance().currentObject = Program::getInstance().sphere;
			Program::getInstance().sphereType = 1;
		}
	}

	if (getInstance().keyPressed['x']) {
		if (Program::getInstance().currentSkybox == Program::getInstance().skybox) {
			Program::getInstance().currentSkybox = Program::getInstance().skybox2;
		}
		else if (Program::getInstance().currentSkybox == Program::getInstance().skybox2) {
			Program::getInstance().currentSkybox = Program::getInstance().skybox3;
		}
		else if (Program::getInstance().currentSkybox == Program::getInstance().skybox3) {
			Program::getInstance().currentSkybox = Program::getInstance().skybox4;
		}
		else if (Program::getInstance().currentSkybox == Program::getInstance().skybox4) {
			Program::getInstance().currentSkybox = Program::getInstance().skybox5;
		}
		else if (Program::getInstance().currentSkybox == Program::getInstance().skybox5) {
			Program::getInstance().currentSkybox = Program::getInstance().skybox;
		}
	}

	glutPostRedisplay();
}

/**
 * @brief Process user keyboard inputs.
 *
 */
void InputManager::processKeyInputs()
{
	// Camera movements
	if (InputManager::getInstance().keyPressed['z'] && InputManager::getInstance().keyPressed['q'])	// Diagonal up left
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::FORWARD_LEFT, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['z'] && InputManager::getInstance().keyPressed['d'])	// Diagonal up right
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::FORWARD_RIGHT, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['s'] && InputManager::getInstance().keyPressed['q'])	// Diagonal down left
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::BACKWARD_LEFT, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['s'] && InputManager::getInstance().keyPressed['d'])	// Diagonal down right
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::BACKWARD_RIGHT, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['z'])
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::FORWARD, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['s'])
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::BACKWARD, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['q'])
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::LEFT, Program::getInstance().deltaTime);
	else if (InputManager::getInstance().keyPressed['d'])
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::RIGHT, Program::getInstance().deltaTime);
	else
		Program::getInstance().camera->ProcessKeyboard(CameraMovement::NONE, Program::getInstance().deltaTime);
}

/**
 * @brief Mouse movement callback on FPS mode
 *
 * @param x X position
 * @param y Y position
 */
void InputManager::mouseMoveFPS(int x, int y)
{
	float xoffset = x - getInstance().mousePositionX;
	float yoffset = getInstance().mousePositionY - y; // reversed since y-coordinates range from bottom to top

	// For rotation when the mouse is close to an edge
	float detectionLimitY = Program::getInstance().windowHeight * 0.2f;
	float detectionLimitX = Program::getInstance().windowWidth * 0.2f;

	getInstance().mousePositionX = static_cast<float>(x);
	getInstance().mousePositionY = static_cast<float>(y);

	// Process the movement with the camera
	Program::getInstance().camera->ProcessMouseMovement(xoffset, yoffset, Program::getInstance().deltaTime);

	glutPostRedisplay();


	// Placing the mouse back to the middle of the screen if at the edges
	float centerX = Program::getInstance().windowHeight / 2.0f;
	float centerY = Program::getInstance().windowHeight / 2.0f;

	if (x < detectionLimitX || x > Program::getInstance().windowWidth - detectionLimitX) {  //you can use values other than 100 for the screen edges if you like, kind of seems to depend on your mouse sensitivity for what ends up working best
		getInstance().mousePositionX = centerX;   //centers the last known position, this way there isn't an odd jump with your cam as it resets
		getInstance().mousePositionY = centerY;
		glutWarpPointer((int)centerX, (int)centerY);  //centers the cursor
	}
	else if (y < detectionLimitY || y > Program::getInstance().windowHeight - detectionLimitY) {
		getInstance().mousePositionX = centerX;
		getInstance().mousePositionY = centerY;
		glutWarpPointer((int)centerX, (int)centerY);
	}
}