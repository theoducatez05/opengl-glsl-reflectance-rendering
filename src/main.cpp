/*****************************************************************//**
 * @file   main.cpp
 * @brief  The main program file
 *
 * Assignement 2 - Real Time Rendering - Trinity College 2021/2022
 *
 * @author theod
 * @date   February 2022
 *********************************************************************/


// General includes
#include <iostream>
#include <vector>
#include <math.h>
#include <iomanip>
#include <sstream>

// GL includes
#include <GL/glew.h>
#include <GL/freeglut.h>

// glm includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// imgui
#include "utils/imgui/imgui.h"
#include "utils/imgui/imgui_impl_glut.h"
#include "utils/imgui/imgui_impl_opengl3.h"

// Project includes
#include "utils/opengl/camera/Camera.h"
#include "utils/opengl/Shader.h"
#include "utils/opengl/GraphicObject.h"
#include "utils/opengl/camera/FreeCamera.h"
#include "utils/InputManager.h"
#include "utils/opengl/Skybox.h"

// Textures for IMGUI
std::unique_ptr<Texture> textureMountainPreview;
std::unique_ptr<Texture> textureSnowPreview;
std::unique_ptr<Texture> textureBeachPreview;
std::unique_ptr<Texture> textureChurchPreview;
std::unique_ptr<Texture> textureNiagaraPreview;

// Objects
std::shared_ptr<GraphicObject> cube;

void showImGui() {
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGLUT_NewFrame();

	ImGui::Begin("Theo Ducatez RR 2");

	// Shader stuff
	ImGui::Dummy(ImVec2(0.0f, 30.0f));
	ImGui::Text("Shader properties:");
	ImGui::Dummy(ImVec2(0.0f, 30.0f));

	static float eta = 1.52f;
	static float chroma[] = { -0.03f, 0.01f, 0.02f };

	ImGui::SliderFloat("ETA", &eta, 1.0f, 50.0f);
	ImGui::Dummy(ImVec2(0.0f, 30.0f));
	ImGui::SliderFloat3("Chromatic Dispertion", chroma, -0.1f, 0.1f);

	ImGui::Dummy(ImVec2(0.0f, 30.0f));
	ImGui::Separator();

	ImGui::Dummy(ImVec2(0.0f, 30.0f));
	ImGui::Text("Objects:");
	ImGui::Dummy(ImVec2(0.0f, 30.0f));

	const char* items[] = {"Sphere", "Cube", "Cone", "Torus", "Diamond", "Teapot" };
	static int item_current_idx = 0;

	if (ImGui::BeginListBox("##listbox 1", ImVec2(-FLT_MIN, 6 * ImGui::GetTextLineHeightWithSpacing())))
	{
		for (int n = 0; n < IM_ARRAYSIZE(items); n++)
		{
			const bool is_selected = (item_current_idx == n);
			if (ImGui::Selectable(items[n], is_selected)) {
				item_current_idx = n;
				if (items[item_current_idx] == "Sphere") {
					Program::getInstance().currentObject = Program::getInstance().sphere;
				}
				else if (items[item_current_idx] == "Cube") {
					Program::getInstance().currentObject = Program::getInstance().cube2;
				}
				else if (items[item_current_idx] == "Cone") {
					Program::getInstance().currentObject = Program::getInstance().cone;
				}
				else if (items[item_current_idx] == "Torus") {
					Program::getInstance().currentObject = Program::getInstance().torus;
				}
				else if (items[item_current_idx] == "Diamond") {
					Program::getInstance().currentObject = Program::getInstance().diamond;
				}
				else {
					Program::getInstance().currentObject = Program::getInstance().teapot;
				}
			}
			// Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
			if (is_selected)
				ImGui::SetItemDefaultFocus();
		}
		ImGui::EndListBox();
	}

	ImGui::Dummy(ImVec2(0.0f, 30.0f));
	ImGui::Separator();

	ImGui::Dummy(ImVec2(0.0f, 30.0f));
	ImGui::Text("Skyboxes:");
	ImGui::Dummy(ImVec2(0.0f, 30.0f));

	ImVec2 size = ImVec2(96.0f, 96.0f);                     // Size of the image we want to make visible
	ImVec2 uv0 = ImVec2(0.0f, 0.0f);                        // UV coordinates for lower-left
	ImVec2 uv1 = ImVec2(1.0f, 1.0f);		// UV coordinates for (32,32) in our texture
	ImVec4 bg_col = ImVec4(0.0f, 0.0f, 0.0f, 1.0f);         // Black background
	ImVec4 tint_col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);       // No tint

	if (ImGui::ImageButton((ImTextureID)textureMountainPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		Program::getInstance().currentSkybox = Program::getInstance().skybox;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((ImTextureID)textureSnowPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		Program::getInstance().currentSkybox = Program::getInstance().skybox5;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((ImTextureID)textureChurchPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		Program::getInstance().currentSkybox = Program::getInstance().skybox2;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((ImTextureID)textureBeachPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		Program::getInstance().currentSkybox = Program::getInstance().skybox3;
	}
	ImGui::SameLine();
	if (ImGui::ImageButton((ImTextureID)textureNiagaraPreview->getId(), size, uv0, uv1, -1, bg_col, tint_col)) {
		Program::getInstance().currentSkybox = Program::getInstance().skybox4;
	}

	ImGui::End();

	Program::getInstance().transmittanceShader->bind();
	Program::getInstance().transmittanceShader->setUniform1f("ratio_o", eta);
	Program::getInstance().transmittanceShader->setUniform1f("r_chromaticDiff", chroma[0]);
	Program::getInstance().transmittanceShader->setUniform1f("g_chromaticDiff", chroma[1]);
	Program::getInstance().transmittanceShader->setUniform1f("b_chromaticDiff", chroma[2]);

	// Rendering
	ImGui::Render();
	ImGuiIO& io = ImGui::GetIO();
	glViewport(0, 0, (GLsizei)io.DisplaySize.x, (GLsizei)io.DisplaySize.y);
	glUseProgram(0);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

/**
 * @brief Glut main diplsay method (used in the main glut loop)
 *
 */
void display()
{
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	cube->drawPhong(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());

	glBindTexture(GL_TEXTURE_CUBE_MAP, Program::getInstance().currentSkybox->getTextureID());
	Program::getInstance().transmittanceShader->bind();
	Program::getInstance().transmittanceShader->setUniform3fv("u_viewPos", Program::getInstance().camera->getPosition());

	Program::getInstance().currentObject->drawSimple(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());

	Program::getInstance().currentSkybox->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());

	showImGui();

	glutSwapBuffers();
}

/**
 * @brief Objects and shader initialization function
 *
 */
void init()
{
	glEnable(GL_DEPTH_TEST);

	// Set up the light object in the scene and its shader
	Program::getInstance().cubeLightShader = std::make_shared<Shader>(
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Light.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Light.frag.glsl");
	cube = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Cube\\Cube.obj", Program::getInstance().cubeLightShader, false);
	cube->getTransform().setPosition({ 0.0f, 5.0f, -7.0f });
	cube->getTransform().setScale({ 0.05f, 0.05f, 0.05f });
	cube->setLit(false);

	// Set up the skyboxes
	Program::getInstance().skybox = std::make_shared<Skybox>(Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Mountains\\",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.frag.glsl");

	Program::getInstance().skybox2 = std::make_shared<Skybox>(Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Church\\",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.frag.glsl");

	Program::getInstance().skybox3 = std::make_shared<Skybox>(Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Beach\\",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.frag.glsl");

	Program::getInstance().skybox4 = std::make_shared<Skybox>(Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Niagara\\",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.frag.glsl");

	Program::getInstance().skybox5 = std::make_shared<Skybox>(Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Snow\\",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.frag.glsl");

	Program::getInstance().currentSkybox = Program::getInstance().skybox;

	Program::getInstance().currentSkybox = Program::getInstance().skybox;

	// Set up the Shader
	Program::getInstance().transmittanceShader = std::make_shared<Shader>(
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Transmittance.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Transmittance.frag.glsl");
	Program::getInstance().transmittanceShader->bind();
	Program::getInstance().transmittanceShader->setUniform3fv("u_viewPos", Program::getInstance().camera->getPosition());
	Program::getInstance().transmittanceShader->setUniform1f("ratio_o", Program::getInstance().currentETA);
	Program::getInstance().transmittanceShader->setUniform1i("skybox", 0);

	// Set up objects
	Program::getInstance().sphere = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Sphere\\Sphere.fbx", Program::getInstance().transmittanceShader, false);

	Program::getInstance().cube2 = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Cube2\\Cube2.fbx", Program::getInstance().transmittanceShader, false);

	Program::getInstance().cone = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Cone\\Cone.fbx", Program::getInstance().transmittanceShader, false);

	Program::getInstance().torus = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Torus\\Torus.fbx", Program::getInstance().transmittanceShader, false);

	Program::getInstance().diamond = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Diamond\\Diamond.fbx", Program::getInstance().transmittanceShader, false);

	Program::getInstance().teapot = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Teapot\\Teapot.fbx", Program::getInstance().transmittanceShader, false);

	Program::getInstance().currentObject = Program::getInstance().sphere;

	// Load gui textures
	textureMountainPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Mountains\\front.jpg").c_str(), TextureType::Diffuse, false);
	textureBeachPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Beach\\front.jpg").c_str(), TextureType::Diffuse, false);
	textureSnowPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Snow\\front.jpg").c_str(), TextureType::Diffuse, false);
	textureNiagaraPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Niagara\\front.jpg").c_str(), TextureType::Diffuse, false);
	textureChurchPreview = std::make_unique<Texture>((Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Church\\front.jpg").c_str(), TextureType::Diffuse, false);
}

/**
 * @brief Process a window resize event
 *
 * @param width The new window width
 * @param height The new window height
 */
void resizeWindow(int width, int height)
{
	InputManager::getInstance().mousePositionX = width / 2.0f;
	InputManager::getInstance().mousePositionY = height / 2.0f;

	Program::getInstance().windowHeight = height;
	Program::getInstance().windowWidth = width;

	glViewport(0, 0, width, height); // reset the viewport
	Program::getInstance().projection = glm::perspective(glm::radians(45.0f), (float)Program::getInstance().windowWidth / Program::getInstance().windowHeight, 0.1f, 5000.0f);
}

/**
 * @brief Glut idle function callback, to update the scene.
 *
 */
void updateScene()
{

	// Update program deltaTime
	Program::getInstance().updateDeltaTime((long)glutGet(GLUT_ELAPSED_TIME));

	// Rotations of the objects
	Program::getInstance().sphere->getTransform().setRotationY(Program::getInstance().sphere->getTransform().getRotationY() + Program::getInstance().deltaTime * .5f);
	Program::getInstance().cube2->getTransform().setRotationY(Program::getInstance().cube2->getTransform().getRotationY() + Program::getInstance().deltaTime * .5f);
	Program::getInstance().cone->getTransform().setRotationY(Program::getInstance().cone->getTransform().getRotationY() + Program::getInstance().deltaTime * .5f);
	Program::getInstance().torus->getTransform().setRotationY(Program::getInstance().torus->getTransform().getRotationY() + Program::getInstance().deltaTime * .5f);
	Program::getInstance().diamond->getTransform().setRotationY(Program::getInstance().diamond->getTransform().getRotationY() + Program::getInstance().deltaTime * .5f);
	Program::getInstance().teapot->getTransform().setRotationY(Program::getInstance().teapot->getTransform().getRotationY() + Program::getInstance().deltaTime * .5f);

	// Process key inputs
	InputManager::getInstance().processKeyInputs();

	// Draw the next frame
	glutPostRedisplay();
}

/**
 * @brief Main function
 *
 * @param argc System arguments count
 * @param argv System arguments
 */
int main(int argc, char** argv)
{
	// Set up the window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(Program::getInstance().windowWidth, Program::getInstance().windowHeight);
	glutCreateWindow("CS7GV03 | Assignment 2 | Theo Ducatez");

	GLenum res = glewInit();

	// Check for any errors
	if (res != GLEW_OK) {
		std::cout << "Error: \n" << glewGetErrorString(res) << std::endl;
		return 1;
	}

	// Set up input configurations and callbacks
	//glutSetCursor(GLUT_CURSOR_NONE);
	glutKeyboardFunc(InputManager::keyPress);
	glutKeyboardUpFunc(InputManager::keyUp);
	//glutPassiveMotionFunc(InputManager::mouseMoveFPS);

	// Register general call back funtions
	glutDisplayFunc(display);
	glutIdleFunc(updateScene);
	glutReshapeFunc(resizeWindow);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();

	// Setup Platform/Renderer backends
	ImGui_ImplGLUT_Init();
	ImGui_ImplGLUT_InstallFuncs();
	ImGui_ImplOpenGL3_Init("#version 330");

	// Setting up the graphic objects
	init();

	// Begin infinite event loop
	glutMainLoop();

	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGLUT_Shutdown();
	ImGui::DestroyContext();
	return 0;
}